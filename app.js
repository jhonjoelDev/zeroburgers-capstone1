const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav__links');
    const navLinks = document.querySelectorAll('.nav__links li');
    const scroll = window.scrollY;

	// When burger icon is click => [1] [2] [3]
	burger.addEventListener('click', () => {



		// nav container slide [1]
		nav.classList.toggle('nav__links-active');

		// links animation [2]
		navLinks.forEach((link, index) => {
			link.style.animation
				? link.style.animation = ''
				: link.style.animation = `navLinkFade .3s ease forwards ${index / 10 + 0.3}s`
		});

		// animate burger [3]
		burger.classList.toggle('toggle');
	});	
};

const onScoll = () => {
	let navContainer = document.querySelector('.nav-container');
	let logo = document.getElementById("logo");
	let logoUrlDk = './assets/images/logo-sm-dk.webp';
	let logoUrlLt = './assets/images/logo-sm-lt.webp';
	let navItem = document.querySelectorAll('.nav__links li');
	let page = document.title;

	// appopriate logo color for different pages
	if (page != 'Zero Burger | Home') {
		logo.src = './assets/images/logo-sm-dk.webp';
	} else {
		logo.src= './assets/images/logo-sm-lt.webp';
	}


	// When not in the top most of page => [1] [2]
	window.addEventListener('scroll', () => {
		// change style of nav container [1]
		navContainer.classList.toggle('scrolling-active', window.scrollY > 0);

		// change logo of nav container if not in mobile view [2]
		if (window.innerWidth > 767) {
			logo.src = logoUrlLt;
			if (page != 'Zero Burger | Home') {
				logo.src = logoUrlDk;
			} else {
				if (window.scrollY > 0) {
					logo.src = logoUrlDk;
				} else if (window.scrollY == 0) {
					logo.src = logoUrlLt;
				}
			}	
		} else {
			logo.src = logoUrlLt;
		}
	});	
}

const hideNav = () => {
	let lastScroll = 0;
	let navbar = document.getElementById('nav-container')

	// when scrolling
	window.addEventListener('scroll', () => {
		let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

		if (scrollTop > lastScroll) {
			navbar.style.top = '-100%';
		} else {
			navbar.style.top = '0';
		}
		lastScroll = scrollTop;
	});
}



hideNav()
onScoll();
navSlide();